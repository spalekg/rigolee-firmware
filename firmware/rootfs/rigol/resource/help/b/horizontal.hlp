﻿<?xml version="1.0" encoding="utf-8"?>
<help>
  <name>horizontal</name>
  <items>
    <item>
      <msg>1284</msg>
      <relation>1280,1294,1295</relation>
      <title>Horizontal menu</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Press this key to turn on the horizontal control menu. 
** To turn on/off the delayed sweep function;
** to change timebase.</content>
    </item>
    <item>
      <msg>1280</msg>
      <relation>1284</relation>
      <title>Time Base</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>This key is used to select X-Y or Y-T time base.
** Y-T : The X-axis shows the time and the Y-axis shows the amplitude
** X-Y: The X-axis shows the CHX amplitude and the Y-axis shows the CHY amplitude.Use the screen corresponding sampling points to draw.
** ROLL: Roll mode causes the waveform to move across the display from right to left.</content>
    </item>
    <item>
      <msg>1312</msg>
      <relation>1284</relation>
      <title>Acquire Mode</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>The Acquisition mode can be: Normal, Averaging and Peak Detect.
** Normal: It makes the oscilloscope to acquire the signals on equal interval time in order to rebulid the waveforms.
**  Averaging: It lets you average multiple triggers together to reduce noise.
** Peak Detect: It helps you find glitches and narrow spikes regardless of sweep speed.
** High Resolution:  Average continuous data into a point, and use all these points to build a waveform. This mode can improve the signal capacity, reduce noise and improve resolution.</content>
    </item>
    <item>
      <msg>1282</msg>
      <relation>1284</relation>
      <title>X-Y</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Change X-Y channel: 
** CH1-CH2</content>
    </item>
    <item>
      <msg>1294</msg>
      <relation>1284</relation>
      <title>Horizontal scale</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>** Use this knob to select the horizontal time/div (scale factor) for the main or the Delayed Scan time base.</content>
    </item>
    <item>
      <msg>1295</msg>
      <relation>1284</relation>
      <title>Horizontal position</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Modify the trigger displacement:
** when the rotary knob is turned, the trigger point moves relative to the center of the screen.
** during the modification process, the waveforms of all channels move around, and the trigger displacement information on the upper right of the screen changes in real time.
** pressing the knob can quickly reset trigger displacement (or delayed scan displacement).</content>
    </item>
    <item>
      <msg>1286</msg>
      <relation>1284</relation>
      <title>Sample rate</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Displays the  sampling rate and the user can not modify it.
** The sampling rate  is related to the channel mode.
** The maximum sampling rate of the single channel mode is 10GSa/s(MSO5000 is 8GSa/s).
** The maximum sampling rate of the dual channel mode is 5GSa/s(MSO5000 is 4GSa/s).
** The maximum sampling rate of the four channel model is 2.5GSa/s(MSO5000 is 2GSa/s).</content>
    </item>
    <item>
      <msg>1287</msg>
      <relation>1284</relation>
      <title>Average Number</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>** set the average number by rotating the multi-function knob.
** values range from 2 to 65536, with an increase of 2.
** the higher the average number of times, the better the noise and resolution will be improved.</content>
    </item>
    <item>
      <msg>1288</msg>
      <relation>1284,1288</relation>
      <title>Memory Depth</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>For MSO&amp;DS7000 series
Set the memory depth of the analog channel. 
** single channel  mode, Auto, 1K, 10K, 100k, 1M, 10M, 25M, 50M, 100M, 125M (Option), 250M (Option), 500M (Option);
** dual channels mode, Auto, 1K, 10K, 100k, 1M, 10M, 25M, 50M, 100M (Option), 125M (Option), 250M (Option);
** four channels mode, Auto, 1K, 10K, 100k, 1M, 10M, 25M, 50M (Option), 100M (Option), 125M (Option). 

For MSO5000 series，NO 500M(5RL) Option
** single channel mode, the storage depth is optional: automatic, 1K, 10K, 100k, 1M, 10M, 25M, 50M, 100M, 200M (Option);
** dual channel mode, storage depth options: automatic, 1K, 10K, 100k, 1M, 10M, 25M, 50M(Option)；
** four channel mode, storage depth optional: automatic, 1K, 10K, 100k, 1M, 10M, 25M(Option)；</content>
    </item>
    <item>
      <msg>1290</msg>
      <relation>1284</relation>
      <title>Anti-aliasing</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Anti aliasing switch
** anti aliasing opens better to avoid signal aliasing.
** when the anti aliasing opens, the refresh rate may decrease.</content>
    </item>
    <item>
      <msg>1281</msg>
      <relation>1284</relation>
      <title>Delayed</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Delayed scan: used to amplify a waveform to facilitate viewing details.
** delay scan time base setting can not be slower than main time base setting.
** delayed scan is based on the minimum time base and the main time base.</content>
    </item>
    <item>
      <msg>1291</msg>
      <relation>1284</relation>
      <title>Fine</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Open or close the fine tune mode:
** fine-tuning mode: time-based stall, step and current stall. In general, it is 1-2-4 order.
** normal mode: time base gear is adjusted according to 1-2-5 order, and the step is larger.</content>
    </item>
    <item>
      <msg>1292</msg>
      <relation>1284,1280,1294,1295</relation>
      <title>Run/Stop</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>** Controls the operation and stop of the waveform sampling.
** In the running state, the key is a yellow background lamp. The upper left corner of the screen shows "T'D", "AUTO" or "WAIT".
** In the stop state, the key is a red background lamp and the upper left corner of the screen displays "STOP".</content>
    </item>
    <item>
      <msg>1302</msg>
      <relation>
      </relation>
      <title>Expand</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Horizontal expansion" refers to the base position of horizontal waveform expansion or compression when adjusting the time base.
** Center: the oscilloscope will expand or compress the waveform around the center of the screen.
** Left: the oscilloscope will expand horizontally or compress the waveform around the left side of the screen.
** Right: the oscilloscope will expand horizontally around the right end of the screen or compress the waveform.
** Trigger point: the oscilloscope will expand or compress the waveform around the trigger point.
** Custom: the oscilloscope will extend or compress the waveform around the user defined reference level.</content>
    </item>
    <item>
      <msg>1326</msg>
      <relation>
      </relation>
      <title>back</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>** Under the YT main time base, the current menu is not available for search or waveform recording and STOP;
** Playing on the memory, playing forward, and setting up 3 speed levels on multiple presses.</content>
    </item>
    <item>
      <msg>1327</msg>
      <relation>
      </relation>
      <title>next</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>** Under the YT main time base, the current menu is not available for search or waveform recording and STOP;
** Playback to memory, backwards, and multiple presses can set 3 speed levels.</content>
    </item>
    <item>
      <msg>1328</msg>
      <relation>
      </relation>
      <title>stop</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>** Under the YT main time base, the current menu is not available for search or waveform recording and STOP;
** Press this button to stop playing.</content>
    </item>
    <item>
      <msg>1293</msg>
      <relation>
      </relation>
      <title>AUTO ROLL</title>
      <key>
      </key>
      <option>
      </option>
      <sibling>
      </sibling>
      <content>Switch to ROLL mode WHEN timebase more than 200ms automatically</content>
    </item>
  </items>
</help>