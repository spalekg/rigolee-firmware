This documentation is designed to provide guidance to customers on how to 
upgrade the MSO/DS7000 series digital oscilloscope to the version in the 
"DS7000(ARM)update.rar" file. 

 
New versions of firmware are available at the following website:
http://int.rigol.com/Support/SoftDownload/3

[Notices]

    - Before upgrading, please carefully refer to the 
      "MSO_DS7000 Release Notes.txt" file to obtain the updated information of
      the firmware version. 
      
    - Please make sure your USB disk can be read correctly. MSO/DS7000 series 
      is required to use a USB Flash drive disk with FAT32 format. 
      
    - During the upgrade process, please do not cut off power and pull out the 
      USB disk, otherwise the instrument will fail to work normally.
      
    - MSO/DS7000 series digital oscilloscope does not support the downgrading
      operations. 

[Upgrading Procedures]

    1. Copy the upgrade file DS7000Update.GEL to the root directory of the USB
       disk. 
       
    2. Keep the instrument in the power-on state, then insert the USB disk into
       the USB HOST interface (next to the Power key). 
       
    3. A help menu is automatically displayed on the interface(Utility->Help), reminding
       you that an upgrade file has been detected. You can click "Local upgrade" to 
       start to upgrade. 
       
    4. During the upgrade, the instrument interface will display the current 
       upgrade progress. 
       
    5. After upgrading has been completed, the instrument will restart. 
    
    6. Check whether the firmware version number has been updated 
       (press Utility -> System -> System Info). 
       
    7. After upgrading has been completed, perform the self-calibration 
       operation. Make sure that the instrument has been warmed up or has been
       operating for more than 30 minutes before the self-calibration. 
       
        a. Disconnect all input channels; 
        
        b. Press Utility->System-> Self-Cal, then click Start to perform the 
           self-calibration. 
           
        c. It takes about 80 minutes to perform the self-calibration. 
        
        d. After the calibration has been completed, restart the instrument.

[Troubleshooting]

    - If the instrument failed to read the upgrade file from the USB disk, 
      please try to replace the USB disk. 
      
    - If the problem still persists, please try to re-download the upgrade file.
    
    - If the problem still persists after performing the above two operations, 
      please visit the website int.rigol.com to find contacts in your area. 